/*Author: Daniel Findlay
Date: Sept 27, 2019
Description: Converts from Celcsius to Fahrenheit
*/

package lab2q2;

import java.util.Scanner;

public class Lab2Q2 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double Celsius, Fahrenheit;
       
        System.out.println("Enter the Temperature in Fahrenheit: ");
        Fahrenheit = input.nextDouble();
        
        Celsius = ((Fahrenheit - 32)*5)/9;
        System.out.println(Fahrenheit + " degrees Fahrenheit = " + Celsius + " degrees Celsius");
    }
}