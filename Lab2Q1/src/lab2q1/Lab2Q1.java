/*Author: Daniel Findlay
Date: Oct 2, 2019
Description: Converts from Celsius to Fahrenheit
*/
package lab2q1;

import java.util.Scanner;
 
public class Lab2Q1 {


    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        double Celsius, Fahrenheit;
       
        System.out.println("Enter the Temperature in Celcius: ");
        Celsius = input.nextDouble();
        
        Fahrenheit = (9.0/5.0)*Celsius + 32;
        System.out.println( Celsius + " degrees Celsius = " + Fahrenheit + " degrees Fahrenheit");
    }
    
}
