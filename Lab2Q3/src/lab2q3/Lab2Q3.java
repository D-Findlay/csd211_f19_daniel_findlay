/*
Author: Daniel Findlay
Date: Sept 30, 2019
Description: Takes the size of a cylinder and finds its volume
*/
package lab2q3;

import java.math.*;
import java.util.Scanner;

public class Lab2Q3 {
  
    public static void main(String[] args) {
        
       Scanner input = new Scanner(System.in);
       
       double CylRad, CylLeng, CylVol, CylArea;
       
       System.out.println("Please enter the Radius of the cylinder: ");
       CylRad = input.nextDouble();
       System.out.println("please enter the radius of the cylinder");
       CylLeng = input.nextDouble();
       CylArea = CylRad * CylRad * Math.PI;
       CylVol = CylArea * CylLeng;
       System.out.println("A cylinder with the radius " + CylRad + " and a length of "+ CylLeng + "has a volume of " +CylVol);
    }
}

       
       
       
       
       
      

